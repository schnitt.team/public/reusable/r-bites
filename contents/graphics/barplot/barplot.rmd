---
title: "Barplot"
output:
  html_document:
    toc: true
    theme: united
---



# Barplots

## Parameters

| Param                  | Brief                                            |
|------------------------|--------------------------------------------------|
| `width`                | width of each bar                                |
| `space`                | space between bars                               |
| `names.arg`            | bar labels, override names of the input variable |
| **General parameters** |                                                  |
| `xlim`, `ylim`         | view port                                        |
| `col`                  | color of the bars                                |
| `add`                  | create new or add to existing                    |
| `las=1`                | horizontal axis labels                           |


# Minimal example

```{r}
barplot(abs(rnorm(4)))
```

By default,

* there is no plot title
* there are no labels for the bars (unless the specified variable has names)
* there is no label for the y-axis



# Parameters


```{r}
n <- 4
x <- abs(rnorm(n))

par(mfrow=c(1, 4))  # 4 sub-plots
barplot(x, width=seq(n), main="width=")
barplot(x, space=seq(n), main="space=")
barplot(x, names.arg = letters[1:n], main="names.arg=")
barplot(x, horiz=T, main="horiz=TRUE")



```

# Multiple series
Barplot can plot multiple series either in groups or on top of each other.


```{r}
n <- 4
df <- data.frame(a = abs(rnorm(n)), b = abs(rnorm(n)))

(x <- as.matrix(df))


par(mfrow=c(1,4))
barplot(x, beside = F, main="beside=FALSE")
barplot(x, beside = T, main="beside=TRUE")

# transpose
(x <- t(x))
colnames(x) <- letters[1:n]
barplot(x, beside = F, main="beside=F, transposed")
barplot(x, beside = T, main="beside=T, transposed")
```


# Samples

## Plain, old barchart
```{r}
# random data
x <- abs(rnorm(round(runif(1, 3, 8))))
# add label names
names(x) <- letters[1:length(x)]
# make ylim higher so that it candisplay label
ylims <- c(0, 1.1*max(x))

# pre-plot
bp <- barplot(x, las=1, ylim=ylims)

abline(h=seq(0, 5, .05), col="lightgray", lty=3)
abline(h=seq(0, 5, .1), col="gray")
bp <- barplot(x, las=1, ylim=ylims, add=T, col="lightblue")


text(bp, x, round(x, 2), pos=3, col="blue")

```

## Vertical barchart
* veritcal bar charts are specified by `horiz=TRUE`
  * bars are _horizontal_ (run from left to right)
  * but the bars are stacked _vertically_

```{r}
n <- 10
x <- abs(rnorm(n))
# scale to [0, 2]
x <- 2*x/max(x)

names(x) <- paste("category", seq(length(x)))
x <- x[order(x)]

par(mar=c(5.1, 6 ,4.1 ,2.1))
xlims = c(0, 2.1)

bp <- barplot(x, horiz=T, las=1, col="lightblue", xlim=xlims)
abline(v=seq(0, 10, .25), col="lightgray", lty=3)
abline(v=seq(0, 10, .5), col="gray")
bp <- barplot(x, horiz=T, las=1, col="lightblue", xlim=xlims, add=T)
text(x, bp, round(x, 2), col="blue", pos=4)


```


## Ratio chart

```{r}
n = 10
a <- sort(abs(rnorm(n)))
a <- .9 * a/max(a)
df <- data.frame(a=a, b=1-a)
x <- t(as.matrix(df))

bp <- barplot(x, col=c("lightblue", "lightgray"), names.arg = seq(n), las=1)
text(bp, x[1,], round(x[1,], 2), col="blue", pos=3)


```
