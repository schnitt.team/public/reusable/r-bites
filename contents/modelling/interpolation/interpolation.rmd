---
title: "Score Interpolation for Classification"
output:
  html_document:
    toc: true
    theme: united
---

# Introduction
End users of machine learning-based systems often require some  means and tools
  to fine tune their systems.
The most common thing is to adjusting some sort of a threshold value in
  a classification task to make it less or more tolerable.
This kind of threshold

However, the raw scores used in algorithms as the basis of classification are not easily comprehensible by users.
Typically, users have no access to such variables.

One way to help this situation, is to provide a familiar scale to the user (e.g. 0: reject everything, 100: accept everything),
  that is an interpolation of the scores used by the model.


This article shows how such interpolation can be done, ina controlled manner.


## The data
Let us pool over two random normal distributions with different means and standard deviation.
These values can be conceptualized as raw scores for positive and negative samples - for example in a speaker
verification task.


```{r}
x <- seq(-100, 100, .1)
xlims=c(-60, 60)

x1 <- rnorm(500, -20, 8)
x2 <- rnorm(500, 10, 8)

hst1 <- hist(x1, breaks=seq(-100, 100, 5), plot=F)
hst2 <- hist(x2, breaks=seq(-102.5, 100, 5), plot=F)

# add canvas with horizontal rulers
ylims <- c(0, ceiling(1.1 * max(c(hst1$counts, hst2$counts)) / 10) * 10)
plot(NA, xlim=xlims, ylim=ylims, xlab="Raw score", ylab="Count", las=1)
abline(h=seq(0, ylims[2], 10), col="lightgray", lty=2)
abline(h=seq(0, ylims[2], 20), col="lightgray")

# plot white bars - to avoid rulers getting through
plot(hst1, xlim=xlims, ylim=ylims, col="white", add=T)
plot(hst2, xlim=xlims, ylim=ylims, col="white", add=T)

# plot histograms with actual colors
plot(hst1, xlim=xlims, ylim=ylims, col=rgb(1,0,0,1/5) , xlab="Score", main="", ylab="", add=T)
plot(hst2, xlim=xlims, ylim=ylims, col=rgb(0,0,1,1/5), add=T)
```


The raw scores - together with corresponding pos/neg labels - can be stored in a single data frame.

```{r}
df <- rbind(data.frame(x=x1, lab=0), data.frame(x=x2, lab=1) )
```


## EER
Errors in a binary classifiation task can be:

  * false positives (**FP**): a negative sample identified as positive
    * e.g., non-authorized user identified as authorized
  * false negatives (**FN**): a positive sample is identified as negative
    * e.g. authorized user access is rejected


The perfomrance of binary classification systems can be characterized by a single value
called _equal error rate_ (**EER**).
Equal error rate represents an operation point of the system (i.e. a threshold setting) where the **rates** for false positives (FPR) and false negatives (FNR) are equal.
It common to calibrate systems to operate at - or relative to - the EER.


Calculating EER is not difficult with R, but the **ROCR** package is very handy.

```{r}
library(ROCR)
pred <- prediction(df$x, df$lab)
roc.perf <- performance(pred, measure="tnr", x.measure="fnr")
```


The error rates for false positive can be visualized easily with the **ROCR** package.
The plot can be interpreted as the vertical axis showing the rates of errors
when moving thre threshold for the binary decision along the horizontal axis.
Moving the threshold higher, results in rejecting more negative samples (lower false positive rate)
while increasing rejection mistakes (higher false negative rate).


```{r}
line.x <- c(x1, x2)
line.x <- line.x[order(line.x)]
line.x <- c(line.x[1] - line.x[2] + line.x[1], line.x)


# create plot-base - without the actual lines
plot(-1000, xlim=c(-50, 30), ylab="%", xlab="", ylim=c(0, 1), las=1)
abline(v=seq(-100, 100, 10), col="lightgray")
abline(h=seq(0, 1, 0.1), col="lightgray", lty=2)
abline(h=seq(0, 1, 0.2), col="lightgray")
abline(h=c(0, 1), col="gray", lwd=2)

fn <- pred@fn[[1]]/max(pred@fn[[1]])
fp <- pred@fp[[1]]/max(pred@fp[[1]])
lines(line.x, fn, type="l", lwd=2, col="red")
lines(line.x, fp, type="l", lwd=2, col="blue")

# Add EER line
false.diff <- abs(fp - fn)
eer.ix <- which(false.diff == min(false.diff))
abline(v=line.x[eer.ix], col="black")
text(line.x[eer.ix], 0.5, pos=4, "EER", col="black")
```

It is common to plot corresponding FNR and FPR data points, which represent characteristics of the system.
This is called Receiver Operation Characteristics, and the line that represents these characteristics is called the **ROC curve**.
ROC curves, and area under the curve (AUC) come handy when comparing different systems (e.g. systems trained on different data),
are not important for the current topic, but for sake of completeness the snippet below shows how to plot an ROC curve.

```{r}
plot(roc.perf, col="blue", lwd=2, main="ROC curve")

grid()
abline(a=1, b=-1, col="gray")
abline(h=1, col="gray")
abline(v=0, col="gray")
```



# Score interpolation
Why is score interpolation needed?

  * to provide means to adjust operation point a classification system
    * this means, that the user can adjust a threshold value that influences the _severity_ of classification performance.
      Typically, higher scores result in more severe, lower score in more forgiving behaviour.
  * to normalize threshold to a specific range (e.g. 0 to 100)
    * the range of raw scores - the basis for classification - is often unknown to the user
    * it is easier to work with a fixed, familiar scale:  $[0, 100]$ can be seen as percentage _(though it is not)_


## Simple math
Interpolation is just a function that provides an output value for an input value.
For classification, it is common to use a sigmoid function,
  which maps the range of all real numbers to the range of $[0, 1]$.
So, what we know is that the interpolated scores should map to a range of $[1, 100]$.

$$x \in \mathbb{R}:   -\infty \le x \le \infty$$

$$f(x) \in \mathbb{R}:  0 \le x \le 100$$


Logistic function a good candidate for the task as it inrepolates its input
  to a specific range - in its original form to $[0, 1$.

$$f(x)=\frac{1}{1+e^{-x}}$$

```{r}
sig.x <- seq(-6, 6, .1)
sig.fx <- 1 / (1 + exp(-sig.x))

plot(sig.x, sig.fx, type="l", col="blue", ylab="interpolated score", xlab="raw score", las=1, lwd=2)
abline(h=seq(0, 1, .2), col="lightgray")
abline(v=seq(-6, 6, 1), col="lightgray")
abline(h=c(0, 1), col="darkgray", lwd=2)
abline(v=0, col="darkgray", lwd=2)
```

Of course, the vanilla logistic function is a bit lacking, its (1) range is different from our goal, (2) its mid-point fixed to zero,
  and the if the variation in the input scores are expected to be between $-6$ and $6$.
The good news is that generalizing the logistic function with some parameters - as explained in the next section - can address these issues.


In order to find a logistic interpolation function, two points have to be fixed.
The number of points to be fixed depends on the type of function to be used.
In our case, two points are enough.
In practice, this means that we need to define two raw-to-interpolated score mappings $x \rightarrow S$.
The selection of exact values are application specific - some guidelines for binary classification are provided in the next section.


In sum, beyond the fixed range of $f(x) \in [0, 100]$, two landmark interpolation points are known.

$$f(x_1) = S_1$$

$$f(x_2) = S_2$$




## Landmark points
In order to find the interpolation function - or to be more precise, to find the parameters for our logistic function - two distinct landmark points have to be defined.
A single point is not enough as it can belong to a wide range of logistic functions.
The two landmark points for the interpolation can be anything.
It can even be decided upon eyeballing the raw score distribution,
  or upon inspecting the distribution of the interpolated scores.

There is no clear ruling on this, but it is recommended, however, to use equal-error-rate as one of the reference point for interpolation.

1. EER
   * EER is a good candidate for a landmark point, as it is used commonly as a point of operation
   * that is, the interpolated score for EER will be the default threshold for the system
     * e.g., the raw score associated with the EER ($x_{EER}$) should map to interpolated value of $80$
     * the default threshold of the system is $80$

$$f(x_{EER})=80$$

2. FP=x
   * another good candidate for a landmark point to define the interpolating function
is a score associated with a specific false positive rate
      * for example, 1% false positive rate should map to interpolated score of 99
   * the other points can also be defined by having an eye on the distribution of interpolated score
      * we would like to avoid distributions concentrating at two opposite poles of the output range
      * we would like to avoid too squeezing both true and false scores into a narrow range
   * for bravity, let us specify the landmark point in an ad-hoc manner as interpolated score $95$ for input raw score $10$

$$x_2 = 10$$
$$f(x_2) = f(10) = 95$$


Note that when specifying the landmark points, it should be confirmed that both $x_1 < x_2$ and  $f(x_1) < f(x_2$).
For example, if the EER is below 1%, it the other point should not be tied to FPR=1%, but to lower value.

## Equation
Below is a generalized equation for sigmoid functions with parameterized maximum value $c$, slope $a$ and mid-point $b$.

$$f(x) =  \frac{c}{1 + e^{-a(x-b)}}$$

The upper limit of interpolated scores is $c = 100$, so only $a$ and $b$ has to be calculated.
So we are solving for $a$ and $b$ using two equations, where values of $c=100$, $x_1$, $x_2$, $f(x_1)$ and $f(x_2)$ are defined.

$$f(x_1) =  \frac{c}{1 + e^{-a(x_1-b)}}$$
$$f(x_2) =  \frac{c}{1 + e^{-a(x_2-b)}}$$


## Derivation

Starting point is the generalized sigmoid. Let us try to solve for $a$ and $b$.

$$f(x) =  \frac{c}{1 + e^{-a(x-b)}}$$


$$f(x)(1 + e^{-a(x-b)}) = c$$

$$\frac{c}{f(x)} = 1 + e^{-a(x-b)}$$

$$log \left( \frac{c}{f(x)} - 1 \right) = -a(x-b)$$


Since $c$ and $x$ are known, we can simplify the left side as a constant.

$$V_1 = log \left( \frac{c}{f(x_1)} - 1 \right)$$

The 2 specified points results in 2 equations.

$$V_1 = a(x_1 - b)$$
$$V_2 = a(x_2 - b)$$

The value for $a$ can be achieved by subtracting the equations.


$$V_1 - V2 = a(x_1 - b) - a(x_2 - b)$$


$$V_1 - V2 = ax_1 - ab - ax_2 + ab$$

$$V_1 - V2 = ax_1 - ax_2$$

$$V_1 - V2 = a(x_1 - x_2)$$

$$a = \frac{V_1 - V2}{x_1 - x_2}$$

The parameter $a$ represents slope: it is a ratio of changes in the y-axis ($V_1 - V_2$) over changes on the x-axis ($x_1 - x_2$).
Parameter $a$ is the tangent specified by the 2 points.


Replacing $V$ with its original value.

$$a = \frac{log \left( \frac{c}{f(x_1)} - 1 \right) - log \left( \frac{c}{f(x_2)} - 1 \right)}{x_1 - x_2}$$


With parameter $a$ known, parameter $b$ can be solved.

$$V_1 = a(x_1 - b)$$

$$\frac{V_1}{a} = x_1 - b$$

$$b = x_1 - \frac{V_1}{a} $$

Variable $V$ can be expanded. Both equations, one with $x_1$, the other with $x_2$, result in the same value.

$$b = x_1 - \frac{log \left( \frac{c}{f(x_1)} - 1 \right)}{a} = x_2 - \frac{log \left( \frac{c}{f(x_2)} - 1 \right)}{a}  $$


## R code
The equations are spelled out as code below.

```{r}
x.1  <- line.x[eer.ix]  # 'x.1' is raw score for EER
fx.1 <- 80              # fx.1 is the interpolated score. We want this to be 80.

x.2  <- 10  # 'x.1' is raw score of 10
fx.2 <- 95

# use 'V1', 'V2' constants to simplify calculations
V1 <- -log(100/fx.1 - 1)
V2 <- -log(100/fx.2 - 1)

a = (V2- V1)/(x.2 - x.1)

# 2 ways for calculating 'b'
b.1 <- x.1 - V1/a
b.2 <- x.2 - V2/a

# the two 'b' values must be equal (as much as floats can be)
stopifnot(abs(b.1 - b.2) < 0.0001)

b <- b.1
```


For convenience, the parameterized sigmoid function can be declared as a user specified function in R.
Parameters of $a$ and $b$ as defined above.

```{r}
sig <- function(x, a, b){
  return( 100 / (1 + exp(-a*(x-b))))
}
```

Check the function declaration against the $x$ -> $f(x)$ landmark points.

```{r}

# check against the 2 specified values
stopifnot(abs(sig(x.1, a, b) - fx.1) < 0.0001)
stopifnot(abs(sig(x.2, a, b) - fx.2) < 0.0001)

```


Let us confirm the interpolation visually by plotting
 the original histograms, the interpolating function with the landmark points.

```{r}
hist.max <- round(1.1*max(c(hst1$counts, hst2$counts))/10)*10
plot(hst1, xlim=xlims, col=rgb(1,0,0,1/5), las=1,
     xlab="Score", main="Score interpolation", ylab="", ylim=c(0, hist.max))
plot(hst2, xlim=xlims, col=rgb(0,0,1,1/5), add=T)

abline(h=hist.max, col="lightgray", lty=1)
abline(h=hist.max*c(fx.1, fx.2)/100, col="gray", lwd=2, lty=2)
abline(v=c(x.1, x.2), col="gray", lty=2)

# plot the interpolating function
lines(x, hist.max*sig(x, a, b)/100, type="l", ylim=c(0, 100), xlim=xlims)
points(c(x.1, x.2), hist.max*c(fx.1, fx.2)/100, col="blue", pch=20, cex=2)

text(x.1, hist.max*c(fx.1)/100, pos=1, "EER", col="blue")
text(x.2, hist.max*c(fx.2)/100, pos=1, "FP=?", col="blue")

axis.at = seq(0, hist.max, length.out=11)
axis.at[9] <- NA
axis(4, at=axis.at, seq(0, 100, 10), las=2, cex.axis=.7)
axis(4, at=hist.max*c(fx.1, fx.2)/100, c(fx.1, fx.2), las=2, col.axis="blue")
```


# Interpolated score distribution
The final results of score interpolation should be checked for the following criteria.

* interpolated distributions ideally should not have more overlap than the original score distribution
* interpolated scores should spread out relatively evenly over the 0-100 range
* interpolated scores should not concentrate around the 2 poles of 0 and 100
   * this results in an _empty_ mid-field,
   * scores tend to _jump_ either to 0 or to 100 jumping scores
   * difficult to tune (e.g., changing threshold just a little bit)


```{r}

df$reg <- sig(df$x, a, b)

hst1 <- hist(df$reg[df$lab==1], breaks=seq(0, 100, 2), plot=F)
hst2 <- hist(df$reg[df$lab==0], breaks=seq(0, 100, 2), plot=F)

ylims <- c(0, ceiling(1.1 * max(c(hst1$counts, hst2$counts)) / 10) * 10)
plot(NA, xlim=c(0, 100), ylim=ylims, las=1,
     xlab="Interpolated score", ylab="Count", main="Interpolated score distribution")
abline(h=seq(0, ylims[2], 10), col="lightgray", lty=2)
abline(h=seq(0, ylims[2], 20), col="lightgray")

# plot white bars - to avoid rulers getting through
plot(hst1, xlim=xlims, ylim=ylims, col="white", add=T)
plot(hst2, xlim=xlims, ylim=ylims, col="white", add=T)

plot(hst1, xlim=c(0, 100), ylim=c(0, y.max), col=rgb(0, 0, 1, 1/5), add=T)
plot(hst2, xlim=c(0, 100), ylim=c(0, y.max), col=rgb(1,0,0, 1/5), add=T)

# Add EER line
abline(v=fx.1, col="red")
text(fx.1, sum(ylims)/2, "EER", pos=4, col="red")


```
