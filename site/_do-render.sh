#!/usr/bin/env bash

# copies contet pages to build dir

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
SCRIPT_NAME=$(basename $0)


PRJ_DIR=$(realpath "${SCRIPT_DIR}/.." )
TS=$(date +"%Y%m%d-%H%M%S")
TS="dev"
BLD_DIR="${PRJ_DIR}/build/${TS}"



mkdir -p $BLD_DIR

pushd $BLD_DIR
cp -R "${PRJ_DIR}/contents" .
cp  "${PRJ_DIR}/site/"* .
rm $SCRIPT_NAME


tree $BLD_DIR

#Rscript -e "rmarkdown::render_site()"
Rscript -e "blogdown::build_site()"
popd


