# R Bites

This is a collection of bits and pieces (and bites) of R that I often use at work.  

**blogdown** versus **rmarkdown**

# Links
* https://rmarkdown.rstudio.com/lesson-13.html : R Markdown websites
* [sample website](https://posit.cloud/content/181978) gnerated by R-markdown
* [R Chart site](https://r-charts.com/correlation/hexbin-chart/) of José Carlos Soage González



# Requirements

* On Ubuntu `r-cran-knitr` package pulls in all required dependencies
```bash
sudo apt-get install r-base 
sudo apt-get install r-cran-knitr
sudo apt-get install pandoc
```

```bash
sudo Rscript -e "install.packages('blogdown',repos='http://cran.rstudio.com')"
Rscript -e "blogdown::install_hugo()"
```
 
